$(document).ready(function () {
  // красивый select
  if ($(".js-select").length) {
    $(".js-select").select2({
      minimumResultsForSearch: Infinity,
      // closeOnSelect: false,
    });
  }
  // маска для инаупов
  if ($("[data-inputmask]").length) {
    $(":input").inputmask();
  }
  // /маска для инпупов
  $(window).resize(function () {
    $(".js-select").select2({
      minimumResultsForSearch: Infinity,
    });
  });
  // /красивый select
  // адаптивное меню
  $('.js-menu-opener').on('click', function() {
    $('.js-menu').toggleClass('open')
    $('.js-menu-inside').slideToggle()
  })
  $('.js-toggle-submenu').on('click', function() {
    console.log(5487)
    $(this).toggleClass('open')
    $(this).parent().find('.js-submenu').slideToggle()
  })
  // /адаптивное меню
  // Реализованные проекты
  var swiper = new Swiper(".js-project", {
    slidesPerView: 2,
    spaceBetween: 30,
    navigation: {
      prevEl: ".js-project-prev",
      nextEl: ".js-project-next",
    },
    pagination: {
      el: ".js-project-pagination",
    },
    breakpoints: {
      0: {
        slidesPerView: 1,
      },
      599: {
        slidesPerView: 1,
      },
      600: {
        slidesPerView: 2,
      },
    },
  });
  // /Реализованные проекты
  // Дилерская сеть
  $('.js-dealer-point').on('click', function(e) {
    e.preventDefault()
    let item = $(this).attr('href')
    if( !$(this).hasClass('active')) {
      $('.js-dealer-point.active').removeClass('active')
      $(this).addClass('active')
      $('.js-dealer-info.active').hide()
      $(item).show().addClass('active')
      console.log(item)
    }
  })
  // /Дилерская сеть
  // Наши клиенты
  var swiper = new Swiper(".js-clients", {
    slidesPerView: 4,
    spaceBetween: 30,
    navigation: {
      prevEl: ".js-clients-prev",
      nextEl: ".js-clients-next",
    },
    pagination: {
      el: ".js-clients-pagination",
    },
    breakpoints: {
      0: {
        slidesPerView: 2,
      },
      499: {
        slidesPerView: 2,
      },
      500: {
        slidesPerView: 3,
      },
      999: {
        slidesPerView: 3,
      },
      1000: {
        slidesPerView: 4,
      },
    },
  });
  // /Наши клиенты
  // левое меню
  $('.js-left-menu-arr').on('click', function(){
    $(this).toggleClass('open')
    $(this).parents('.js-left-menu-li').find('.js-left-menu-sub').slideToggle()
  })
  // /левое меню
})